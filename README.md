## Basic example using onClick events with menu and buttons  

## For use ButterKnife  

#### Gradle (Module: app)  

Add depedencies:  
* compile 'com.jakewharton:butterknife:8.8.1'  
* annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'  

#### In Activity  

* import butterknife.BindView;  
* import butterknife.ButterKnife;  

###### onCreate()  

Add line:  
* ButterKnife.bind(this);  

#### For View elements  

Use annotation  
@BindView(R.id.id_name) KindOfElement NameOfElement;  

For listeners  
 @OnClick (R.id.id_name)  
public void function() { }  

and others...

