package com.mefisto.menuapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,
        View.OnClickListener {

    private FABToolbarLayout morph;
    private TextView textoEditable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        morph = (FABToolbarLayout) findViewById(R.id.fabtoolbar);
        textoEditable = (TextView) findViewById(R.id.listPrints);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        morph.show();
                        textoEditable.append("\nSe toco el fab");
                    }
                });


        View copy, paste, cut, select;

        copy = findViewById(R.id.copy);
        paste = findViewById(R.id.paste);
        cut = findViewById(R.id.cut);
        select = findViewById(R.id.select);

        copy.setOnClickListener(this);
        paste.setOnClickListener(this);
        cut.setOnClickListener(this);
        select.setOnClickListener(this);


        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.copy:
                textoEditable.append("\nSe toco el boton de copiar");
                break;
            case R.id.paste:
                textoEditable.append("\nSe toco el boton de pegar");
                break;
            case R.id.cut:
                textoEditable.append("\nSe toco el boton de cortar");
                break;
            case R.id.select:
                textoEditable.append("\nSe toco el boton de seleccionar");
        }
        morph.hide();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.a1:
                textoEditable.append("\nSe tocó el menú acción 1");
                break;
            case R.id.a2:
                textoEditable.append("\nSe tocó el menú acción 2");
                break;
            case R.id.a3:
                textoEditable.append("\nSe tocó el menú acción 3");
                break;
            case R.id.o1:
                textoEditable.append("\nSe tocó el menú opción 1");
                break;
            case R.id.o2:
                textoEditable.append("\nSe tocó el menú opción 2");
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
